# Stage installation

## Remove old stage installation

    >> sudo apt-get remove ros-indigo-stage
    
## Clone stage 

    >> git clone gitgate@mas.b-it-center.de:amr-lab/stage.git

## Build and Install
    >> mkdir build && cd build
    >> make
    >> cmake ..
    >> sudo make install
    IMP: note down the path of the stage header: stage.hh 
        e.g. /usr/local/include/Stage-4.1/stage.hh
    IMP: note down the path of the stage library : libstage.so.4.1.1
        e.g. /usr/local/lib64/libstage.so.4.1.1
        
## Create symlink of the stage library with ROS

    sudo ln -s /usr/local/lib64/libstage.so.4.1.1 /opt/ros/indigo/lib64/libstage.so
    sudo ln -s /usr/local/lib64/libstage.so.4.1.1 /opt/ros/indigo/lib64/libstage.so.4.1.1
    sudo ln -s /usr/local/lib64/libstage.so.4.1.1 /opt/ros/indigo/lib/libstage.so
    sudo ln -s /usr/local/lib64/libstage.so.4.1.1 /opt/ros/indigo/lib/libstage.so.4.1.1
    
## Copy header file to ROS Stage-4.1

    sudo cp /usr/local/include/Stage-4.1/stage.hh /opt/ros/indigo/include/Stage-4.1/stage.hh
    
# Test AMR Stage
## Setup ROS workspace and clone amr lab test

    >> cd ~/catkin_ws/src
    >> git clone git@github.com:ivan-vishniakou/amr_lab_test.git
    >> cd ~/catkin_ws/
    >> catkin build
    Terminal 1 >> roslaunch amr_stage stage.launch
    Terminal 2 >> rosrun amr_ui teleop_console.py
    Note: Move the robot with presented control interface
